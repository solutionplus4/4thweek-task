import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notes:  JSON.parse(localStorage.getItem('notes'))? JSON.parse(localStorage.getItem('notes')):[ ] ,
  },
  getters: {},
  mutations: {
    addNote(state,note){
      note.id = state.notes.length;
      state.notes.push(note);
      localStorage.setItem('notes',JSON.stringify(state.notes));
      console.log(state.notes);
    },
    updateNote(state,{id,title,description}){
      if (title){
        state.notes[id].title = title;
      }
      if(description){
        state.notes[id].description = description;
      }
      localStorage.setItem('notes', JSON.stringify(state.notes));
      console.log(state.notes);
    },
    deleteNote(state,id){
      //state.notes.splice(state.notes[id],1);
      state.notes[id].deleted = true;
      state.notes = state.notes.filter(note => !note.deleted);

      state.notes.forEach((note, index) => {
        note.id = index;
      });

      localStorage.setItem('notes', JSON.stringify(state.notes));
      console.log(state.notes);
    },
    updateCompleted(state,id){
      state.notes[id].completed = !state.notes[id].completed;
      localStorage.setItem('notes', JSON.stringify(state.notes));
    },
    toggleDescription(state,id){
      state.notes[id].showDescription = !state.notes[id].showDescription;
    },
  },
  actions: {
    doAddNote(context,note){
      context.commit('addNote',note);
    },
    doUpdateNote(context,{id,title,description}){
      context.commit('updateNote',{id,title,description});
    },
    doUpdateCompleted(context,id){
      context.commit('updateCompleted',id);
    },
    doDeleteNote(context,id){
      context.commit('deleteNote',id);
    },
    doToggleDescription(context,id){
      context.commit('toggleDescription',id);
    },
  },
  modules: {},
});
